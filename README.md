![logo](https://i.imgur.com/vFFBz0h.png)
# <p align="center">OpenVK 2.0</p>
**A brand new update. \[*status: in developing*\]**
## TODO
- [x] Multilanguage (*at the moment the project is translated into three languages \[English, Russian, Ukrainian\]*)
- [ ] AJAX
- [x] Posts
- [ ] Comments for posts
- [ ] Audio
- [x] User page
- [x] Groups and communities
- [x] Friends
- [ ] Videos
- [x] Settings
- [ ] Albums with photos
- [ ] Notifications (*when we implement AJAX-loading*)
- [ ] Page with last posts
- [ ] Personal messages and conversations
- [ ] Admin panel
- [ ] The script for installing and configuring OpenVK
- [x] Theme support
- [ ] Plugin support
- [x] *Nice sprites ~~and scary monsters~~ ever :)*